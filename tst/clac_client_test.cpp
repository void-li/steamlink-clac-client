/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <utility>

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include <QByteArray>
#include <QFile>
#include <QHostAddress>
#include <QHostInfo>
#include <QIODevice>
#include <QList>
#include <QMetaType>
#include <QObject>
#include <QString>
#include <QVariant>

#include "clac.pb.hpp"
#include "clac_client.hpp"
#include "config.hpp"
#include "packets.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

using namespace lvd::steamlink;
using namespace lvd::steamlink::clac;

// ----------

Q_DECLARE_METATYPE(QHostAddress)

// ----------

#define CAPTURE_STDOUT(X, Y) {                                                 \
  QFile osout("stdout.log");                                                   \
  osout.open(QIODevice::WriteOnly);                                            \
                                                                               \
  QFile isout("stdout.log");                                                   \
  isout.open(QIODevice:: ReadOnly);                                            \
                                                                               \
  int saved_handle = dup(fileno(stdout));                                      \
                                                                               \
  { int ret = dup2(osout.handle(), fileno(stdout));                            \
    Q_ASSERT(ret >= 0); }                                                      \
                                                                               \
  { X }                                                                        \
                                                                               \
  { int ret = dup2(saved_handle  , fileno(stdout));                            \
    Q_ASSERT(ret >= 0); }                                                      \
                                                                               \
  { Y }                                                                        \
                                                                               \
  osout.close();                                                               \
  isout.close();                                                               \
                                                                               \
  QFile::remove("stdout.log"); }

// ----------

class ClacClientTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void message() {
    QFETCH(quint32, type);

    QFETCH(QString, hostname);
    QFETCH(QString, statekey);

    QFETCH(bool   , idle);

    QTextStream* qStdOutPtr = nullptr;
    std::swap(qStdOutPtr, lvd::__impl__::qStdOutPtr);

    LVD_FINALLY {
      lvd::__impl__::qStdOutPtr = qStdOutPtr;
    };

    ClacClient clac_client;

    CAPTURE_STDOUT({
      proto::DaemonMessagePacket::Message message;
      message.setType(static_cast<proto::DaemonMessage::Type>(type));
      message.setHostname(hostname);

      proto::DaemonMessagePacket daemon_message_packet(message);
      clac_client.on_message(daemon_message_packet.serialize());
    }, {
      QByteArray messages = isout.readAll();
      QVERIFY(messages.contains(statekey.toUtf8()));
    })

    if (idle) {
      CAPTURE_STDOUT({
        QSignalSpy spy(&clac_client, &ClacClient::destroyed);
        QVERIFY(spy.isValid());

        spy.wait(config::Clac_Client_Idle_Time() + 16);
      }, {
        if        (statekey ==    "connected_event") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("connected"));
        } else if (statekey ==    "connected_other_event") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("connected_other"));
        } else if (statekey == "disconnected_event") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("disconnected"));
        } else if (statekey == "connection_failed") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("disconnected"));
        } else if (statekey == "connection_doomed") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("disconnected"));
        } else if (statekey == "shutdown") {
          auto messages = isout.readAll();
          QVERIFY(messages.contains("none"));
        }
      })
    }
  }

  void message_data() {
    QTest::addColumn<quint32>("type");

    QTest::addColumn<QString>("hostname");
    QTest::addColumn<QString>("statekey");

    QTest::addColumn<bool   >("idle");

    QTest::addRow("connected")
        << static_cast<quint32>(proto::DaemonMessage::   CONNECTED)
        << QHostInfo::localHostName() << "connected" << false;

    QTest::addRow("connected_other")
        << static_cast<quint32>(proto::DaemonMessage::   CONNECTED)
        << QHostInfo::localHostName() + "some" << "connected_other"
        << false;

    QTest::addRow("connected_event")
        << static_cast<quint32>(proto::DaemonMessage::   CONNECTED_TO)
        << QHostInfo::localHostName() << "connected_event"
        << true;

    QTest::addRow("connected_other_event")
        << static_cast<quint32>(proto::DaemonMessage::   CONNECTED_TO)
        << QHostInfo::localHostName() + "some" << "connected_other_event"
        << true;

    QTest::addRow("disconnected")
        << static_cast<quint32>(proto::DaemonMessage::DISCONNECTED)
        << QHostInfo::localHostName() << "disconnected"
        << false;

    QTest::addRow("disconnected_from")
        << static_cast<quint32>(proto::DaemonMessage::DISCONNECTED_FROM)
        << QHostInfo::localHostName() << "disconnected_from"
        << true;

    QTest::addRow("connection_failed")
        << static_cast<quint32>(proto::DaemonMessage::CONNECTION_FAILED)
        << QHostInfo::localHostName() << "connection_failed"
        << true;

    QTest::addRow("connection_doomed")
        << static_cast<quint32>(proto::DaemonMessage::CONNECTION_DOOMED)
        << QHostInfo::localHostName() << "connection_doomed"
        << true;

    QTest::addRow("shutdown")
        << static_cast<quint32>(proto::DaemonMessage::SHUTDOWN)
        << QHostInfo::localHostName() << "shutdown"
        << true;
  }

  // ----------

  void sigusr1() {
    ClacClient clac_client;

    QSignalSpy spy(&clac_client, &ClacClient::transmit);
    QVERIFY(spy.isValid());

    raise(SIGUSR1);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    proto::ClientMessagePacket client_message_packet(spy[0][0].toByteArray());
    QVERIFY(client_message_packet.valid());

    proto::ClientMessage       client_message = client_message_packet.message();

    QCOMPARE(client_message.type(),     proto::ClientMessage::CONNECT_TO);
    QCOMPARE(client_message.hostname(), QHostInfo::localHostName());
  }

  void sigusr1_while_connected() {
    ClacClient clac_client;

    QSignalSpy spy(&clac_client, &ClacClient::transmit);
    QVERIFY(spy.isValid());

    {
      proto::DaemonMessage       daemon_message;
      daemon_message.setType(proto::DaemonMessage::CONNECTED_TO);
      daemon_message.setHostname(QHostInfo::localHostName() + "some");

      proto::DaemonMessagePacket daemon_message_packet(daemon_message);
      clac_client.on_message(daemon_message_packet.serialize());
    }

    raise(SIGUSR1);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    proto::ClientMessagePacket client_message_packet(spy[0][0].toByteArray());
    QVERIFY(client_message_packet.valid());

    proto::ClientMessage       client_message = client_message_packet.message();

    QCOMPARE(client_message.type(),     proto::ClientMessage::DISCONNECT_FROM);
    QCOMPARE(client_message.hostname(), QHostInfo::localHostName());
  }

  void sigusr2() {
    ClacClient clac_client;

    QSignalSpy spy(&clac_client, &ClacClient::transmit);
    QVERIFY(spy.isValid());

    raise(SIGUSR2);

    QVERIFY (spy.wait(256));
    QCOMPARE(spy.size(), 1);

    proto::ClientMessagePacket client_message_packet(spy[0][0].toByteArray());
    QVERIFY(client_message_packet.valid());

    proto::ClientMessage       client_message = client_message_packet.message();

    QCOMPARE(client_message.type(),     proto::ClientMessage::SHUTDOWN);
    QCOMPARE(client_message.hostname(), QHostInfo::localHostName());
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();

    qRegisterMetaType<QHostAddress>();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ClacClientTest)
#include "clac_client_test.moc"  // IWYU pragma: keep
