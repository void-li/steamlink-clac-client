/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "packets.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <QtEndian>
#include <QtGlobal>

// ----------

#define ASSURE_SIZE(X)                                   \
  if (position +  static_cast<int>(X) > packet.size()) { \
    LVD_LOG_W() << "premature packet end";               \
    return;                                              \
  }; nullptr

#define APPEND_SIZE(X)                                   \
      position += static_cast<int>(X)

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

ClientMessagePacket::ClientMessagePacket(const QByteArray& packet) {
  if (!packet.startsWith(signature)) {
    LVD_LOG_W() << "signature detection failed";
    return;
  }

  qint32 message_size;

  bool ok;
  int position = signature.size();

  ASSURE_SIZE(sizeof(message_size));
  std::memcpy(&message_size, packet.data() + position, sizeof(message_size));
  APPEND_SIZE(sizeof(message_size));

  message_size = qFromLittleEndian(message_size);

  ASSURE_SIZE(       message_size );
  message_ = Message(packet, position, message_size, &ok);
  APPEND_SIZE(       message_size );

  if (!ok) {
    LVD_LOG_W() << "message deserialization failed";
    return;
  }

  valid_ = true;
}

ClientMessagePacket::ClientMessagePacket(const Message& message)
    : message_(message) {
  valid_ = true;
}

QByteArray ClientMessagePacket::serialize() const {
  QByteArray packet(signature);

  QByteArray message      = message_.serialize();
  qint32     message_size = message.size();

  qToLittleEndian(message_size, &message_size);

  packet.append(reinterpret_cast<char*>(&message_size), sizeof(message_size));
  packet.append(message);

  return packet;
}

const QByteArray ClientMessagePacket::signature =
    QByteArray::fromHex("ff ff ff ff 21 4c 5f b0");

// ----------

DaemonMessagePacket::DaemonMessagePacket(const QByteArray& packet) {
  if (!packet.startsWith(signature)) {
    LVD_LOG_W() << "signature detection failed";
    return;
  }

  qint32 message_size;

  bool ok;
  int position = signature.size();

  ASSURE_SIZE(sizeof(message_size));
  std::memcpy(&message_size, packet.data() + position, sizeof(message_size));
  APPEND_SIZE(sizeof(message_size));

  message_size = qFromLittleEndian(message_size);

  ASSURE_SIZE(       message_size );
  message_ = Message(packet, position, message_size, &ok);
  APPEND_SIZE(       message_size );

  if (!ok) {
    LVD_LOG_W() << "message deserialization failed";
    return;
  }

  valid_ = true;
}

DaemonMessagePacket::DaemonMessagePacket(const Message& message)
    : message_(message) {
  valid_ = true;
}

QByteArray DaemonMessagePacket::serialize() const {
  QByteArray packet(signature);

  QByteArray message      = message_.serialize();
  qint32     message_size = message.size();

  qToLittleEndian(message_size, &message_size);

  packet.append(reinterpret_cast<char*>(&message_size), sizeof(message_size));
  packet.append(message);

  return packet;
}

const QByteArray DaemonMessagePacket::signature =
    QByteArray::fromHex("ff ff ff ff 21 4c 5f b1");

// ----------

SteamlinkPacket::SteamlinkPacket(const QByteArray& packet) {
  if (!packet.startsWith(signature)) {
    LVD_LOG_W() << "signature detection failed";
    return;
  }

  qint32 header_size;
  qint32 status_size;

  bool ok;
  int position = signature.size();

  ASSURE_SIZE(sizeof(header_size));
  std::memcpy(&header_size, packet.data() + position, sizeof(header_size));
  APPEND_SIZE(sizeof(header_size));

  header_size = qFromLittleEndian(header_size);

  ASSURE_SIZE(       header_size );
  header_ = Header(packet, position, header_size, &ok);
  APPEND_SIZE(       header_size );

  if (!ok) {
    LVD_LOG_W() << "header deserialization failed";
    return;
  }

  ASSURE_SIZE(sizeof(status_size));
  std::memcpy(&status_size, packet.data() + position, sizeof(status_size));
  APPEND_SIZE(sizeof(status_size));

  status_size = qFromLittleEndian(status_size);

  ASSURE_SIZE(       status_size );
  status_ = Status(packet, position, status_size, &ok);
  APPEND_SIZE(       status_size );

  if (!ok) {
    LVD_LOG_W() << "status deserialization failed";
    return;
  }

  valid_ = true;
}

SteamlinkPacket::SteamlinkPacket(const Header& header,
                                 const Status& status)
    : header_(header),
      status_(status) {
  valid_ = true;
}

QByteArray SteamlinkPacket::serialize() const {
  QByteArray packet(signature);

  QByteArray header      = header_.serialize();
  qint32     header_size = header.size();

  qToLittleEndian(header_size, &header_size);

  packet.append(reinterpret_cast<char*>(&header_size), sizeof(header_size));
  packet.append(header);

  QByteArray status      = status_.serialize();
  qint32     status_size = status.size();

  qToLittleEndian(status_size, &status_size);

  packet.append(reinterpret_cast<char*>(&status_size), sizeof(status_size));
  packet.append(status);

  return packet;
}

const QByteArray SteamlinkPacket::signature =
    QByteArray::fromHex("ff ff ff ff 21 4c 5f a0");

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
