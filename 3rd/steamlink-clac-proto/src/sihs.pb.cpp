#include <QSharedData>
#include <QtGlobal>

#include "sihs.pb.hpp"
#include "sihs.pb.h"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

bool ERemoteClientBroadcastMsg_IsValid(int value) {
  return __pqpp__::ERemoteClientBroadcastMsg_IsValid(value);
}

// ----------

class CMsgRemoteClientBroadcastHeader::__Data__ : public QSharedData {
 public:
  __pqpp__::CMsgRemoteClientBroadcastHeader d_;
};

// ----------

CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader()
    : d_(new __Data__()) {}

CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader(const QByteArray                               & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader(const QByteArray                               & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader(const __pqpp__::CMsgRemoteClientBroadcastHeader& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader(const CMsgRemoteClientBroadcastHeader&  value) = default;
CMsgRemoteClientBroadcastHeader::CMsgRemoteClientBroadcastHeader(      CMsgRemoteClientBroadcastHeader&& value) = default;
CMsgRemoteClientBroadcastHeader::~CMsgRemoteClientBroadcastHeader()                                             = default;

CMsgRemoteClientBroadcastHeader& CMsgRemoteClientBroadcastHeader::operator= (const CMsgRemoteClientBroadcastHeader&  value) = default;
CMsgRemoteClientBroadcastHeader& CMsgRemoteClientBroadcastHeader::operator= (      CMsgRemoteClientBroadcastHeader&& value) = default;

QByteArray
CMsgRemoteClientBroadcastHeader::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::CMsgRemoteClientBroadcastHeader& CMsgRemoteClientBroadcastHeader::__pqpp__() const {
  return d_->d_;
}

// ----------

quint64 CMsgRemoteClientBroadcastHeader::client_id() const {
  return d_->d_.client_id();
}

bool CMsgRemoteClientBroadcastHeader::hasClient_id() const {
  return true;
}

void CMsgRemoteClientBroadcastHeader::setClient_id(quint64 value) {
  d_->d_.set_client_id(value);
}

void CMsgRemoteClientBroadcastHeader::clearClient_id() {
  d_->d_.clear_client_id();
}

// ----------

ERemoteClientBroadcastMsg CMsgRemoteClientBroadcastHeader::msg_type() const {
  return static_cast<ERemoteClientBroadcastMsg>(d_->d_.msg_type());
}

bool CMsgRemoteClientBroadcastHeader::hasMsg_type() const {
  return d_->d_.has_msg_type();
}

void CMsgRemoteClientBroadcastHeader::setMsg_type(ERemoteClientBroadcastMsg value) {
  d_->d_.set_msg_type(static_cast<__pqpp__::ERemoteClientBroadcastMsg>(value));
}

void CMsgRemoteClientBroadcastHeader::clearMsg_type() {
  d_->d_.clear_msg_type();
}

// ----------

quint64 CMsgRemoteClientBroadcastHeader::instance_id() const {
  return d_->d_.instance_id();
}

bool CMsgRemoteClientBroadcastHeader::hasInstance_id() const {
  return true;
}

void CMsgRemoteClientBroadcastHeader::setInstance_id(quint64 value) {
  d_->d_.set_instance_id(value);
}

void CMsgRemoteClientBroadcastHeader::clearInstance_id() {
  d_->d_.clear_instance_id();
}

// ----------

class CMsgRemoteClientBroadcastStatus_User::__Data__ : public QSharedData {
 public:
  __pqpp__::CMsgRemoteClientBroadcastStatus_User d_;
};

// ----------

CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User()
    : d_(new __Data__()) {}

CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User(const QByteArray                                    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User(const QByteArray                                    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User(const __pqpp__::CMsgRemoteClientBroadcastStatus_User& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User(const CMsgRemoteClientBroadcastStatus_User&  value) = default;
CMsgRemoteClientBroadcastStatus_User::CMsgRemoteClientBroadcastStatus_User(      CMsgRemoteClientBroadcastStatus_User&& value) = default;
CMsgRemoteClientBroadcastStatus_User::~CMsgRemoteClientBroadcastStatus_User()                                                  = default;

CMsgRemoteClientBroadcastStatus_User& CMsgRemoteClientBroadcastStatus_User::operator= (const CMsgRemoteClientBroadcastStatus_User&  value) = default;
CMsgRemoteClientBroadcastStatus_User& CMsgRemoteClientBroadcastStatus_User::operator= (      CMsgRemoteClientBroadcastStatus_User&& value) = default;

QByteArray
CMsgRemoteClientBroadcastStatus_User::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::CMsgRemoteClientBroadcastStatus_User& CMsgRemoteClientBroadcastStatus_User::__pqpp__() const {
  return d_->d_;
}

// ----------

quint64 CMsgRemoteClientBroadcastStatus_User::steamid() const {
  return d_->d_.steamid();
}

bool CMsgRemoteClientBroadcastStatus_User::hasSteamid() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus_User::setSteamid(quint64 value) {
  d_->d_.set_steamid(value);
}

void CMsgRemoteClientBroadcastStatus_User::clearSteamid() {
  d_->d_.clear_steamid();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus_User::auth_key_id() const {
  return d_->d_.auth_key_id();
}

bool CMsgRemoteClientBroadcastStatus_User::hasAuth_key_id() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus_User::setAuth_key_id(quint32 value) {
  d_->d_.set_auth_key_id(value);
}

void CMsgRemoteClientBroadcastStatus_User::clearAuth_key_id() {
  d_->d_.clear_auth_key_id();
}

// ----------

class CMsgRemoteClientBroadcastStatus::__Data__ : public QSharedData {
 public:
  __pqpp__::CMsgRemoteClientBroadcastStatus d_;
};

// ----------

CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus()
    : d_(new __Data__()) {}

CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus(const QByteArray                               & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus(const QByteArray                               & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus(const __pqpp__::CMsgRemoteClientBroadcastStatus& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus(const CMsgRemoteClientBroadcastStatus&  value) = default;
CMsgRemoteClientBroadcastStatus::CMsgRemoteClientBroadcastStatus(      CMsgRemoteClientBroadcastStatus&& value) = default;
CMsgRemoteClientBroadcastStatus::~CMsgRemoteClientBroadcastStatus()                                             = default;

CMsgRemoteClientBroadcastStatus& CMsgRemoteClientBroadcastStatus::operator= (const CMsgRemoteClientBroadcastStatus&  value) = default;
CMsgRemoteClientBroadcastStatus& CMsgRemoteClientBroadcastStatus::operator= (      CMsgRemoteClientBroadcastStatus&& value) = default;

QByteArray
CMsgRemoteClientBroadcastStatus::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::CMsgRemoteClientBroadcastStatus& CMsgRemoteClientBroadcastStatus::__pqpp__() const {
  return d_->d_;
}

// ----------

qint32 CMsgRemoteClientBroadcastStatus::version() const {
  return d_->d_.version();
}

bool CMsgRemoteClientBroadcastStatus::hasVersion() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setVersion(qint32 value) {
  d_->d_.set_version(value);
}

void CMsgRemoteClientBroadcastStatus::clearVersion() {
  d_->d_.clear_version();
}

// ----------

qint32 CMsgRemoteClientBroadcastStatus::min_version() const {
  return d_->d_.min_version();
}

bool CMsgRemoteClientBroadcastStatus::hasMin_version() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setMin_version(qint32 value) {
  d_->d_.set_min_version(value);
}

void CMsgRemoteClientBroadcastStatus::clearMin_version() {
  d_->d_.clear_min_version();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus::connect_port() const {
  return d_->d_.connect_port();
}

bool CMsgRemoteClientBroadcastStatus::hasConnect_port() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setConnect_port(quint32 value) {
  d_->d_.set_connect_port(value);
}

void CMsgRemoteClientBroadcastStatus::clearConnect_port() {
  d_->d_.clear_connect_port();
}

// ----------

QString CMsgRemoteClientBroadcastStatus::hostname() const {
  return QString::fromStdString(d_->d_.hostname());
}

bool CMsgRemoteClientBroadcastStatus::hasHostname() const {
  return !d_->d_.hostname().empty();
}

void CMsgRemoteClientBroadcastStatus::setHostname(const QString& value) {
  d_->d_.set_hostname(value.toStdString());
}

void CMsgRemoteClientBroadcastStatus::clearHostname() {
  d_->d_.clear_hostname();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus::enabled_services() const {
  return d_->d_.enabled_services();
}

bool CMsgRemoteClientBroadcastStatus::hasEnabled_services() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setEnabled_services(quint32 value) {
  d_->d_.set_enabled_services(value);
}

void CMsgRemoteClientBroadcastStatus::clearEnabled_services() {
  d_->d_.clear_enabled_services();
}

// ----------

qint32 CMsgRemoteClientBroadcastStatus::ostype() const {
  return d_->d_.ostype();
}

bool CMsgRemoteClientBroadcastStatus::hasOstype() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setOstype(qint32 value) {
  d_->d_.set_ostype(value);
}

void CMsgRemoteClientBroadcastStatus::clearOstype() {
  d_->d_.clear_ostype();
}

// ----------

bool CMsgRemoteClientBroadcastStatus::is64bit() const {
  return d_->d_.is64bit();
}

bool CMsgRemoteClientBroadcastStatus::hasIs64bit() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setIs64bit(bool value) {
  d_->d_.set_is64bit(value);
}

void CMsgRemoteClientBroadcastStatus::clearIs64bit() {
  d_->d_.clear_is64bit();
}

// ----------

QList<CMsgRemoteClientBroadcastStatus_User> CMsgRemoteClientBroadcastStatus::users()          const {
  QList<CMsgRemoteClientBroadcastStatus_User> value;

  for (int i = 0; i < usersSize(); i ++) {
    value.append(users(i));
  }

  return value;
}

                                      CMsgRemoteClientBroadcastStatus_User  CMsgRemoteClientBroadcastStatus::users(int index) const {
  return User(d_->d_.users(index));
}

bool CMsgRemoteClientBroadcastStatus::hasUsers() const {
  return usersSize() > 0;
}

void CMsgRemoteClientBroadcastStatus::setUsers(           const QList<CMsgRemoteClientBroadcastStatus_User>& value) {
  clearUsers();

  for (auto& vitem : value) {
    addUsers(vitem);
  }
}

void CMsgRemoteClientBroadcastStatus::setUsers(int index, const                                       User & value) {
  (*d_->d_.mutable_users(index)) = value.__pqpp__();
}

void CMsgRemoteClientBroadcastStatus::addUsers(           const                                       User & value) {
  d_->d_.add_users();
  setUsers(usersSize() - 1, value);
}

void CMsgRemoteClientBroadcastStatus::clearUsers() {
  d_->d_.clear_users();
}

int  CMsgRemoteClientBroadcastStatus::usersSize() const {
  return d_->d_.users_size();
}

// ----------

qint32 CMsgRemoteClientBroadcastStatus::euniverse() const {
  return d_->d_.euniverse();
}

bool CMsgRemoteClientBroadcastStatus::hasEuniverse() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setEuniverse(qint32 value) {
  d_->d_.set_euniverse(value);
}

void CMsgRemoteClientBroadcastStatus::clearEuniverse() {
  d_->d_.clear_euniverse();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus::timestamp() const {
  return d_->d_.timestamp();
}

bool CMsgRemoteClientBroadcastStatus::hasTimestamp() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setTimestamp(quint32 value) {
  d_->d_.set_timestamp(value);
}

void CMsgRemoteClientBroadcastStatus::clearTimestamp() {
  d_->d_.clear_timestamp();
}

// ----------

bool CMsgRemoteClientBroadcastStatus::screen_locked() const {
  return d_->d_.screen_locked();
}

bool CMsgRemoteClientBroadcastStatus::hasScreen_locked() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setScreen_locked(bool value) {
  d_->d_.set_screen_locked(value);
}

void CMsgRemoteClientBroadcastStatus::clearScreen_locked() {
  d_->d_.clear_screen_locked();
}

// ----------

bool CMsgRemoteClientBroadcastStatus::games_running() const {
  return d_->d_.games_running();
}

bool CMsgRemoteClientBroadcastStatus::hasGames_running() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setGames_running(bool value) {
  d_->d_.set_games_running(value);
}

void CMsgRemoteClientBroadcastStatus::clearGames_running() {
  d_->d_.clear_games_running();
}

// ----------

QStringList CMsgRemoteClientBroadcastStatus::mac_addresses()          const {
  QStringList value;

  for (int i = 0; i < mac_addressesSize(); i ++) {
    value.append(mac_addresses(i));
  }

  return value;
}

QString     CMsgRemoteClientBroadcastStatus::mac_addresses(int index) const {
  return QString::fromStdString(d_->d_.mac_addresses(index));
}

bool CMsgRemoteClientBroadcastStatus::hasMac_addresses() const {
  return mac_addressesSize() > 0;
}

void CMsgRemoteClientBroadcastStatus::setMac_addresses(           const QStringList& value) {
  clearMac_addresses();

  for (auto& vitem : value) {
    addMac_addresses(vitem);
  }
}

void CMsgRemoteClientBroadcastStatus::setMac_addresses(int index, const QString    & value) {
  d_->d_.set_mac_addresses(index, value.toStdString());
}

void CMsgRemoteClientBroadcastStatus::addMac_addresses(           const QString    & value) {
  d_->d_.add_mac_addresses(       value.toStdString());
}

void CMsgRemoteClientBroadcastStatus::clearMac_addresses() {
  d_->d_.clear_mac_addresses();
}

int  CMsgRemoteClientBroadcastStatus::mac_addressesSize() const {
  return d_->d_.mac_addresses_size();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus::download_lan_peer_group() const {
  return d_->d_.download_lan_peer_group();
}

bool CMsgRemoteClientBroadcastStatus::hasDownload_lan_peer_group() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setDownload_lan_peer_group(quint32 value) {
  d_->d_.set_download_lan_peer_group(value);
}

void CMsgRemoteClientBroadcastStatus::clearDownload_lan_peer_group() {
  d_->d_.clear_download_lan_peer_group();
}

// ----------

bool CMsgRemoteClientBroadcastStatus::broadcasting_active() const {
  return d_->d_.broadcasting_active();
}

bool CMsgRemoteClientBroadcastStatus::hasBroadcasting_active() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setBroadcasting_active(bool value) {
  d_->d_.set_broadcasting_active(value);
}

void CMsgRemoteClientBroadcastStatus::clearBroadcasting_active() {
  d_->d_.clear_broadcasting_active();
}

// ----------

bool CMsgRemoteClientBroadcastStatus::vr_active() const {
  return d_->d_.vr_active();
}

bool CMsgRemoteClientBroadcastStatus::hasVr_active() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setVr_active(bool value) {
  d_->d_.set_vr_active(value);
}

void CMsgRemoteClientBroadcastStatus::clearVr_active() {
  d_->d_.clear_vr_active();
}

// ----------

quint32 CMsgRemoteClientBroadcastStatus::content_cache_port() const {
  return d_->d_.content_cache_port();
}

bool CMsgRemoteClientBroadcastStatus::hasContent_cache_port() const {
  return true;
}

void CMsgRemoteClientBroadcastStatus::setContent_cache_port(quint32 value) {
  d_->d_.set_content_cache_port(value);
}

void CMsgRemoteClientBroadcastStatus::clearContent_cache_port() {
  d_->d_.clear_content_cache_port();
}

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
