#include <QSharedData>
#include <QtGlobal>

#include "clac.pb.hpp"
#include "clac.pb.h"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {
namespace proto {

bool ClientMessage::Type_IsValid(int value) {
  return __pqpp__::ClientMessage::Type_IsValid(value);
}

// ----------

class ClientMessage::__Data__ : public QSharedData {
 public:
  __pqpp__::ClientMessage d_;
};

// ----------

ClientMessage::ClientMessage()
    : d_(new __Data__()) {}

ClientMessage::ClientMessage(const QByteArray             & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

ClientMessage::ClientMessage(const QByteArray             & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

ClientMessage::ClientMessage(const __pqpp__::ClientMessage& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

ClientMessage::ClientMessage(const ClientMessage&  value) = default;
ClientMessage::ClientMessage(      ClientMessage&& value) = default;
ClientMessage::~ClientMessage()                           = default;

ClientMessage& ClientMessage::operator= (const ClientMessage&  value) = default;
ClientMessage& ClientMessage::operator= (      ClientMessage&& value) = default;

QByteArray
ClientMessage::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::ClientMessage& ClientMessage::__pqpp__() const {
  return d_->d_;
}

// ----------

ClientMessage::Type ClientMessage::type() const {
  return static_cast<Type>(d_->d_.type());
}

bool ClientMessage::hasType() const {
  return Type_IsValid(type());
}

void ClientMessage::setType(Type value) {
  d_->d_.set_type(static_cast<__pqpp__::ClientMessage::Type>(value));
}

void ClientMessage::clearType() {
  d_->d_.clear_type();
}

// ----------

QString ClientMessage::hostname() const {
  return QString::fromStdString(d_->d_.hostname());
}

bool ClientMessage::hasHostname() const {
  return !d_->d_.hostname().empty();
}

void ClientMessage::setHostname(const QString& value) {
  d_->d_.set_hostname(value.toStdString());
}

void ClientMessage::clearHostname() {
  d_->d_.clear_hostname();
}

// ----------

bool DaemonMessage::Type_IsValid(int value) {
  return __pqpp__::DaemonMessage::Type_IsValid(value);
}

// ----------

class DaemonMessage::__Data__ : public QSharedData {
 public:
  __pqpp__::DaemonMessage d_;
};

// ----------

DaemonMessage::DaemonMessage()
    : d_(new __Data__()) {}

DaemonMessage::DaemonMessage(const QByteArray             & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

DaemonMessage::DaemonMessage(const QByteArray             & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

DaemonMessage::DaemonMessage(const __pqpp__::DaemonMessage& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

DaemonMessage::DaemonMessage(const DaemonMessage&  value) = default;
DaemonMessage::DaemonMessage(      DaemonMessage&& value) = default;
DaemonMessage::~DaemonMessage()                           = default;

DaemonMessage& DaemonMessage::operator= (const DaemonMessage&  value) = default;
DaemonMessage& DaemonMessage::operator= (      DaemonMessage&& value) = default;

QByteArray
DaemonMessage::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::DaemonMessage& DaemonMessage::__pqpp__() const {
  return d_->d_;
}

// ----------

DaemonMessage::Type DaemonMessage::type() const {
  return static_cast<Type>(d_->d_.type());
}

bool DaemonMessage::hasType() const {
  return Type_IsValid(type());
}

void DaemonMessage::setType(Type value) {
  d_->d_.set_type(static_cast<__pqpp__::DaemonMessage::Type>(value));
}

void DaemonMessage::clearType() {
  d_->d_.clear_type();
}

// ----------

QString DaemonMessage::hostname() const {
  return QString::fromStdString(d_->d_.hostname());
}

bool DaemonMessage::hasHostname() const {
  return !d_->d_.hostname().empty();
}

void DaemonMessage::setHostname(const QString& value) {
  d_->d_.set_hostname(value.toStdString());
}

void DaemonMessage::clearHostname() {
  d_->d_.clear_hostname();
}

}  // namespace proto
}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
