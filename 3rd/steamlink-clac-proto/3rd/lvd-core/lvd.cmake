#
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#

if(NOT DEFINED LVD)
  set(LVD ON)

  if(NOT PROJECT_NAME_LOWER)
    string(TOLOWER "${PROJECT_NAME}" PROJECT_NAME_LOWER)
  endif()

  set(VERSION_FROM_LVD "${LVD_VERSION}")

  if(IS_DIRECTORY "${CMAKE_SOURCE_DIR}/.git")
    find_package(Git)

    if(GIT_FOUND)
      execute_process(COMMAND git describe
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        OUTPUT_VARIABLE VERSION_FROM_GIT
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif()
  endif()

  if(    VERSION_FROM_LVD AND      VERSION_FROM_GIT)
    if(NOT VERSION_FROM_LVD STREQUAL VERSION_FROM_GIT)
      message(WARNING "Lvd and Git version differ!")
    endif()
  endif()

  foreach(VERSION_FROM "${VERSION_FROM_LVD}" "${VERSION_FROM_GIT}")
    if(VERSION_FROM)
      string(REGEX MATCH "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
        VERSION_FROM_MATCH "${VERSION_FROM}")

      if(VERSION_FROM_MATCH)
        if(NOT DEFINED PROJECT_VERSION_MAJOR AND CMAKE_MATCH_COUNT GREATER_EQUAL 1)
          string(REGEX REPLACE "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
            "\\1" PROJECT_VERSION_MAJOR "${VERSION_FROM}")
        endif()

        if(NOT DEFINED PROJECT_VERSION_MINOR AND CMAKE_MATCH_COUNT GREATER_EQUAL 2)
          string(REGEX REPLACE "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
            "\\2" PROJECT_VERSION_MINOR "${VERSION_FROM}")
        endif()

        if(NOT DEFINED PROJECT_VERSION_PATCH AND CMAKE_MATCH_COUNT GREATER_EQUAL 3)
          string(REGEX REPLACE "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
            "\\3" PROJECT_VERSION_PATCH "${VERSION_FROM}")
        endif()

        if(NOT DEFINED PROJECT_VERSION_TWEAK AND CMAKE_MATCH_COUNT GREATER_EQUAL 5)
          string(REGEX REPLACE "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
            "\\5" PROJECT_VERSION_TWEAK "${VERSION_FROM}")
        endif()

        if(NOT DEFINED PROJECT_VERSION_ANNEX AND CMAKE_MATCH_COUNT GREATER_EQUAL 4)
          string(REGEX REPLACE "^v([0-9]+).([0-9]+).([0-9]+)(|-([0-9]+)-[a-z0-9]+)$"
            "\\4" PROJECT_VERSION_ANNEX "${VERSION_FROM}")
        endif()

        if(NOT DEFINED PROJECT_VERSION OR PROJECT_VERSION STREQUAL "")
          foreach(VERSION_IDENT MAJOR MINOR PATCH TWEAK)
            set(VERSION_VALUE "${PROJECT_VERSION_${VERSION_IDENT}}")

            if(NOT VERSION_VALUE STREQUAL "")
              if(NOT DEFINED PROJECT_VERSION OR PROJECT_VERSION STREQUAL "")
                set(PROJECT_VERSION                    "${VERSION_VALUE}")
              else()
                set(PROJECT_VERSION "${PROJECT_VERSION}.${VERSION_VALUE}")
              endif()
            endif()
          endforeach()
        endif()

        break()
      endif()
    endif()
  endforeach()

  if(NOT DEFINED LVD_VERSION OR LVD_VERSION STREQUAL "")
    set(LVD_VERSION "v${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}${PROJECT_VERSION_ANNEX}")
  endif()

  if(NOT DEFINED LVD_VERSION OR LVD_VERSION STREQUAL "")
    set(LVD_VERSION "~unknown")
  endif()

  # ----------

  if(    CMAKE_BUILD_TYPE STREQUAL "Release")
    get_property(LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)

    if("C"   IN_LIST LANGUAGES)
      if(CMAKE_C_COMPILER_ID   STREQUAL "Clang")
        set(LVD_CLANG TRUE)
      endif()
    endif()

    if("CXX" IN_LIST LANGUAGES)
      if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        set(LVD_CLANG TRUE)
      endif()
    endif()

    if(LVD_CLANG)
      find_program(LD_LLD "ld.lld")

      if(LD_LLD)
        add_link_options("-fuse-ld=lld")
      endif()

      include(CheckIPOSupported)
      check_ipo_supported(RESULT LD_LTO)

      if(LD_LLD AND LD_LTO)
        set(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)
      endif()
    endif()
  endif()

  # ----------

  include(GNUInstallDirs)

  if(EXISTS "${CMAKE_INSTALL_FULL_SYSCONFDIR}/os-release")
    file  (READ "${CMAKE_INSTALL_FULL_SYSCONFDIR}/os-release" OS_RELEASE)
    string(FIND "${OS_RELEASE}"                     "ID=arch" OS_RELEASE_ARCH)

    if(OS_RELEASE_ARCH GREATER_EQUAL 0)
      string(REPLACE "lib64" "lib" CMAKE_INSTALL_LIBDIR      "${CMAKE_INSTALL_LIBDIR}")
      string(REPLACE "lib64" "lib" CMAKE_INSTALL_FULL_LIBDIR "${CMAKE_INSTALL_FULL_LIBDIR}")
    endif()
  endif()

  string(REPLACE "var/" "" CMAKE_INSTALL_RUNDIR      "${CMAKE_INSTALL_RUNSTATEDIR}")
  string(REPLACE "var/" "" CMAKE_INSTALL_FULL_RUNDIR "${CMAKE_INSTALL_FULL_RUNSTATEDIR}")

  # ----------

  add_definitions(-DQT_MESSAGELOGCONTEXT)

  # ----------

  if(NOT CMAKE_BUILD_TYPE STREQUAL "Release")
    get_property(LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)

    if("C"   IN_LIST LANGUAGES)
      if(CMAKE_C_COMPILER_ID   STREQUAL "Clang" AND CMAKE_C_COMPILER   MATCHES "clazy$")
        set(LVD_CLAZY TRUE)
      endif()
    endif()

    if("CXX" IN_LIST LANGUAGES)
      if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER MATCHES "clazy$")
        set(LVD_CLAZY TRUE)
      endif()
    endif()

    if(LVD_CLAZY)
      add_definitions(-Xclang -plugin-arg-clazy)
      add_definitions(-Xclang level0,level1,no-connect-by-name,no-fully-qualified-moc-types)
    endif()
  endif()

  # ----------

  if(NOT CMAKE_BUILD_TYPE STREQUAL "Release" AND CMAKE_BUILD_TYPE STREQUAL "Coverage")
    get_property(LANGUAGES GLOBAL PROPERTY ENABLED_LANGUAGES)

    if("C"   IN_LIST LANGUAGES)
      if(NOT CMAKE_C_COMPILER_ID   STREQUAL "Clang")
        message(FATAL_ERROR   "c compiler must be clang")
      endif()
    endif()

    if("CXX" IN_LIST LANGUAGES)
      if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        message(FATAL_ERROR "c++ compiler must be clang")
      endif()
    endif()

    set(CMAKE_C_FLAGS_COVERAGE
      "${CMAKE_C_FLAGS_DEBUG}   -fprofile-arcs -ftest-coverage")

    set(CMAKE_CXX_FLAGS_COVERAGE
      "${CMAKE_CXX_FLAGS_DEBUG} -fprofile-arcs -ftest-coverage")

    set(CMAKE_EXE_LINKER_FLAGS_COVERAGE
      "${CMAKE_EXE_LINKER_FLAGS_DEBUG} --coverage")

    find_program(LCOV_PATH lcov)
    if(NOT LCOV_PATH)
      message(FATAL_ERROR "lcov not found")
    endif()

    find_program(LLVM_PATH llvm-cov)
    if(NOT LLVM_PATH)
      message(FATAL_ERROR "llvm-cov not found")
    endif()

    find_program(GENH_PATH genhtml)
    if(NOT GENH_PATH)
      message(FATAL_ERROR "genhtml not found")
    endif()

    if(NOT EXISTS "${CMAKE_BINARY_DIR}/gcov.sh")
      file(APPEND "${CMAKE_BINARY_DIR}/gcov.sh" "#!/bin/bash\n")
      file(APPEND "${CMAKE_BINARY_DIR}/gcov.sh" "exec llvm-cov gcov \"\$@\"\n")

      execute_process(COMMAND
        chmod 755 "${CMAKE_BINARY_DIR}/gcov.sh"
      )
    endif()

    function(add_coverage targetname targetpath runnername)
      string(REPLACE ";" " " rarguments "${ARGN}")
      set(command "${runnername} ${rarguments}")

      add_custom_target(${targetname} true
        # remove lcov coverage data from prior runs
        COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR} --base-directory . -q -z

        # create lcov coverage data
        COMMAND bash -c "${command}"

        # create lcov output
        COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR} --base-directory . -q -c
          --directory "${CMAKE_SOURCE_DIR}" --no-external
          --gcov-tool "${CMAKE_BINARY_DIR}/gcov.sh"
          -o "${targetpath}.info"

        # remove automake source
        COMMAND ${LCOV_PATH} --remove "${targetpath}.info" "'${CMAKE_BINARY_DIR}/*'"
          -q -o "${targetpath}.inew"
        COMMAND ${CMAKE_COMMAND} -E rename "${targetpath}.inew" "${targetpath}.info"

        # remove 3rdparty source
        COMMAND ${LCOV_PATH} --remove "${targetpath}.info" "'*3rd/*'"
          -q -o "${targetpath}.inew"
        COMMAND ${CMAKE_COMMAND} -E rename "${targetpath}.inew" "${targetpath}.info"

        # remove protobuf source
        COMMAND ${LCOV_PATH} --remove "${targetpath}.info" "'*.pb.*'"
          -q -o "${targetpath}.inew"
        COMMAND ${CMAKE_COMMAND} -E rename "${targetpath}.inew" "${targetpath}.info"

        # remove autogen  source
        COMMAND ${LCOV_PATH} --remove "${targetpath}.info" "'*autogen/*'"
          -q -o "${targetpath}.inew"
        COMMAND ${CMAKE_COMMAND} -E rename "${targetpath}.inew" "${targetpath}.info"

        # create html output
        COMMAND ${GENH_PATH} "${targetpath}.info"
          -q -o "${targetpath}"

        # remove lcov output
        COMMAND ${CMAKE_COMMAND} -E remove "${targetpath}.info"

        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        COMMENT "Create coverage report in '${targetpath}'"
      )

      add_custom_command(TARGET ${targetname} POST_BUILD
        COMMAND true
        COMMENT "Create coverage report in '${targetpath}' finished"
      )
    endfunction()
  else()
    function(add_coverage targetname targetpath runnername)
      add_custom_target(${targetname} false
        COMMENT "Coverage reports require CMAKE_BUILD_TYPE=Coverage"
      )
    endfunction()
  endif()
endif()
