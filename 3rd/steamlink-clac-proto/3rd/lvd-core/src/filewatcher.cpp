/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "filewatcher.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QStringList>

#include "shield.hpp"

// ----------

namespace lvd {

Filewatcher::Filewatcher(const QString& path,
                         QObject*       parent)
    : QObject(parent) {
  LVD_LOG_T() << path;

  watcher_ = new QFileSystemWatcher(this);

  connect(watcher_, &QFileSystemWatcher::directoryChanged,
          this, &Filewatcher::on_dirs_changed);

  connect(watcher_, &QFileSystemWatcher::     fileChanged,
          this, &Filewatcher::on_file_changed);

  interval_ = Interval_;

  watimer_ = new QTimer(this);
  initialize_watimer();

  connect(watimer_, &QTimer::timeout,
          this, &Filewatcher::on_timeout);

  QFileInfo qfileinfo(path);
  path_ = qfileinfo.filePath();
  base_ = qfileinfo.    path();

  if      (!setup_dirs()) {
    if (watimer_->interval() > 0) {
      watimer_->start();
      return;
    }
  }
  else if (!setup_file()) {
//    if (watimer_->interval() > 0) {
//      watimer_->start(); NO!
//      return;
//    }
  }
}

Filewatcher::~Filewatcher() {
  LVD_LOG_T() << path_;

  close_file();
  close_dirs();
}

// ----------

bool Filewatcher::setup_dirs() {
  LVD_LOG_T() << base_;

  if (!watching_dirs()) {
    if (   !QDir (base_).exists()
        || !watcher_->addPath(base_)) {
      LVD_LOG_D() << "setup dirs failure"
                  << base_;

      return false;
    }
  }

  LVD_LOG_D() << "setup dirs success"
              << base_;

  return true;
}

void Filewatcher::close_dirs() {
  LVD_LOG_T() << base_;

  if ( watching_dirs()) {
    LVD_LOG_D() << "closed dirs"
                << base_;

    watcher_->removePath(base_);
  }
}

bool Filewatcher::watching_dirs() const {
  return watcher_->directories().contains(base_);
}

bool Filewatcher::setup_file() {
  LVD_LOG_T() << path_;

  if (!watching_file()) {
    if (   !QFile(path_).exists()
        || !watcher_->addPath(path_)) {
      LVD_LOG_D() << "setup file failure"
                  << path_;

      return false;
    } else {
      emit changed();
    }
  }

  LVD_LOG_D() << "setup file success"
              << path_;

  return true;
}

void Filewatcher::close_file() {
  LVD_LOG_T() << path_;

  if ( watching_file()) {
    LVD_LOG_D() << "closed path"
                << path_;

    watcher_->removePath(path_);
  }
}

bool Filewatcher::watching_file() const {
  return watcher_->files()      .contains(path_);
}

void Filewatcher::initialize_watimer() {
  Q_ASSERT(watimer_);

  bool active = watimer_->isActive();

  if (active) { watimer_-> stop(); }
  watimer_->setInterval(interval_);
  if (active) { watimer_->start(); }
}

// ----------

void Filewatcher::on_dirs_changed() {
  LVD_LOG_T() << base_;
  LVD_SHIELD;

  if (!watching_dirs()) {
    if (!setup_dirs()) {
      if (watimer_->interval() > 0) {
        watimer_->start();
        return;
      }
    }
  }

  if (!watching_file()) {
    if (!setup_file()) {
//      if (watimer_->interval() > 0) {
//        watimer_->start(); NO!
//        return;
//      }
    }
  }

  LVD_SHIELD_END;
}

void Filewatcher::on_file_changed() {
  LVD_LOG_T() << path_;
  LVD_SHIELD;

  if (QFile::exists(path_)) {
    LVD_LOG_D() << "file changed"
                << path_;

    emit changed();
  } else {
    LVD_LOG_D() << "file removed"
                << path_;

    emit removed();
  }

  if (!watching_file()) {
    if (!setup_file()) {
//      if (watimer_->interval() > 0) {
//        watimer_->start(); NO!
//        return;
//      }
    }
  }

  LVD_SHIELD_END;
}

void Filewatcher::on_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  if (!watching_dirs()) {
    if (!setup_dirs()) {
//      if (watimer_->interval() > 0) {
//        watimer_->start(); NO!
//        return;
//      }
      return;
    }
  }

  if (!watching_file()) {
    if (!setup_file()) {
//      if (watimer_->interval() > 0) {
//        watimer_->start(); NO!
//        return;
//      }

//      return; NO!
    }
  }

  watimer_->stop();

  LVD_SHIELD_END;
}

// ----------

const int Filewatcher::Interval_ = 2048;

}  // namespace lvd
