/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QMetaObject>
#include <QObject>
#include <QString>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"
#include "lvd/shield.hpp"

#include "clac_client.hpp"
#include "clac_client_network.hpp"
#include "config.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::steamlink::clac::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::steamlink::clac::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::steamlink::clac::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::steamlink::clac::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  try {
    lvd::steamlink::clac::ClacClient        clac_client;
    lvd::steamlink::clac::ClacClientNetwork clac_client_network;

    QObject::connect(&clac_client_network, &lvd::steamlink::clac::ClacClientNetwork::message,
                     &clac_client,         &lvd::steamlink::clac::ClacClient::on_message);

    QObject::connect(&clac_client,         &lvd::steamlink::clac::ClacClient::transmit,
                     &clac_client_network, &lvd::steamlink::clac::ClacClientNetwork::transmit);

    QMetaObject::invokeMethod(&app, [&] {
      LVD_SHIELD;
      clac_client_network.setup();
      LVD_SHIELD_END;
    }, Qt::QueuedConnection);

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
