/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "clac_client.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QHostInfo>
#include <QTimer>
#include <QVariant>

#include "lvd/shield.hpp"

#include "clac.pb.hpp"
#include "config.hpp"
#include "packets.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

ClacClient::ClacClient(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  settings_watcher_ = new Filewatcher(config::Settings_Path(), this);
  settings_         = new QSettings(config::Settings_Path(), QSettings::IniFormat);

  connect(settings_watcher_, &Filewatcher::changed,
          settings_, [settings = settings_] {
    settings->sync();
  });

  none_timer_ = new QTimer(this);
  none_timer_->setInterval(config::Clac_Client_None_Time());
  none_timer_->setSingleShot(true);

  connect(none_timer_, &QTimer::timeout,
          this, &ClacClient::on_none_timeout);

  idle_timer_ = new QTimer(this);
  idle_timer_->setInterval(config::Clac_Client_Idle_Time());
  idle_timer_->setSingleShot(true);

  connect(idle_timer_, &QTimer::timeout,
          this, &ClacClient::on_idle_timeout);

  sigusr1_ = new Signal(SIGUSR1, this);
  connect(sigusr1_, &Signal::activated,
          this, &ClacClient::on_sigusr1);

  sigusr2_ = new Signal(SIGUSR2, this);
  connect(sigusr2_, &Signal::activated,
          this, &ClacClient::on_sigusr2);

  QTimer::singleShot(1024, this, [&] {
    proto::ClientMessage       client_message;
    client_message.setType(proto::ClientMessage::QUERYING);
    client_message.setHostname(QHostInfo::localHostName());

    proto::ClientMessagePacket client_message_packet(client_message);
    emit transmit(client_message_packet.serialize());
  });

  print_state();
}

ClacClient::~ClacClient() {
  LVD_LOG_T();
}

// ----------

void ClacClient::on_message(const QByteArray& message) {
  LVD_LOG_T() << message.toHex(' ');
  LVD_SHIELD;

  auto daemon_packet = proto::DaemonMessagePacket(message);
  if (!daemon_packet.valid()) {
    LVD_LOG_W() << "received invalid daemon packet";
    return;
  }

  QString hostname = daemon_packet.message().hostname();
  if (hostname.isEmpty()) {
    if (   daemon_packet.message().type() != proto::DaemonMessage::DISCONNECTED
        && daemon_packet.message().type() != proto::DaemonMessage::SHUTDOWN) {
      LVD_LOG_W() << "received invalid daemon packet missing hostname";
      return;
    }
  }

  cancel_none();
  cancel_idle();

  LVD_FINALLY {
    await_none();
  };

  auto& daemon_message = daemon_packet.message();
  switch (daemon_message.type()) {
    case proto::DaemonMessage::   CONNECTED:
      if (daemon_message.hostname() == QHostInfo::localHostName()) {
        state_ = State::CONNECTED;
        print_state();
      } else {
        state_ = State::CONNECTED_OTHER;
        print_state();
      }
      break;

    case proto::DaemonMessage::   CONNECTED_TO:
      if (daemon_message.hostname() == QHostInfo::localHostName()) {
        state_ = State::CONNECTED_EVENT;
        print_state();
      } else {
        state_ = State::CONNECTED_OTHER_EVENT;
        print_state();
      }

      await_idle();
      break;

    case proto::DaemonMessage::DISCONNECTED:
      state_ = State::DISCONNECTED;
      print_state();
      break;

    case proto::DaemonMessage::DISCONNECTED_FROM:
      state_ = State::DISCONNECTED_EVENT;
      print_state();

      await_idle();
      break;

    case proto::DaemonMessage::CONNECTION_FAILED:
      state_ = State::CONNECTION_FAILED;
      print_state();

      await_idle();
      break;

    case proto::DaemonMessage::CONNECTION_DOOMED:
      state_ = State::CONNECTION_DOOMED;
      print_state();

      await_idle();
      break;

    case proto::DaemonMessage::SHUTDOWN:
      state_ = State::SHUTDOWN;
      print_state();

      await_idle();
      break;

    default:
      LVD_LOG_W() << "unknown daemon message type"
                  << daemon_message.type();

      break;
  }

  LVD_SHIELD_END;
}

// ----------

void ClacClient::print_state() {
  LVD_LOG_T();

  QString message_key = state_.to_string();
  message_key = message_key.toLower();

  QString message     = settings_->value(message_key, message_key).toString();
  qStdOut() << message << Qt::endl;
}

// ----------

void ClacClient::on_none_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  state_ = State::NONE;
  print_state();

  LVD_SHIELD_END;
}

void ClacClient::on_idle_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  switch (state_) {
    case State::   CONNECTED_EVENT:
      state_ = State::   CONNECTED;
      print_state();
      break;

    case State::   CONNECTED_OTHER_EVENT:
      state_ = State::   CONNECTED_OTHER;
      print_state();
      break;

    case State::DISCONNECTED_EVENT:
      state_ = State::DISCONNECTED;
      print_state();
      break;

    case State::CONNECTION_FAILED:
      state_ = State::DISCONNECTED;
      print_state();
      break;

    case State::CONNECTION_DOOMED:
      state_ = State::DISCONNECTED;
      print_state();
      break;

    case State::SHUTDOWN:
      state_ = State::NONE;
      print_state();
      break;

    default:
      Q_ASSERT(false);
      break;
  }

  LVD_SHIELD_END;
}

void ClacClient::on_sigusr1() {
  LVD_LOG_T();
  LVD_SHIELD;

  proto::ClientMessage       client_message;

  LVD_LOG_D() << "state"
              << state_.to_string();

  if (   state_ != State::CONNECTED
      && state_ != State::CONNECTED_EVENT
      && state_ != State::CONNECTED_OTHER
      && state_ != State::CONNECTED_OTHER_EVENT) {
    client_message.setType(proto::ClientMessage::   CONNECT_TO  );
    client_message.setHostname(QHostInfo::localHostName());
  } else {
    client_message.setType(proto::ClientMessage::DISCONNECT_FROM);
    client_message.setHostname(QHostInfo::localHostName());
  }

  proto::ClientMessagePacket client_message_packet(client_message);
  emit transmit(client_message_packet.serialize());

  LVD_SHIELD_END;
}

void ClacClient::on_sigusr2() {
  LVD_LOG_T();
  LVD_SHIELD;

  proto::ClientMessage       client_message;
  client_message.setType(proto::ClientMessage::SHUTDOWN);
  client_message.setHostname(QHostInfo::localHostName());

  proto::ClientMessagePacket client_message_packet(client_message);
  emit transmit(client_message_packet.serialize());

  LVD_SHIELD_END;
}

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
