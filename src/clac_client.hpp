/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QHostAddress>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QTimer>

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"
#include "lvd/filewatcher.hpp"
#include "lvd/logger.hpp"
#include "lvd/signal.hpp"

// ----------

namespace lvd {
namespace steamlink {
namespace clac {

class ClacClient : public QObject {
  Q_OBJECT LVD_LOGGER

  LVD_ENUM(State,
    NONE,
       CONNECTED,
       CONNECTED_EVENT,
       CONNECTED_OTHER,
       CONNECTED_OTHER_EVENT,
    DISCONNECTED,
    DISCONNECTED_EVENT,
    CONNECTION_FAILED,
    CONNECTION_DOOMED,
    SHUTDOWN
  );

 public:
  ClacClient(QObject* parent = nullptr);
  ~ClacClient();

 public slots:
  void on_message(const QByteArray& message);

 signals:
  void transmit(const QByteArray&   message,
                const QHostAddress& address = QHostAddress::Broadcast);

 private:
  void await_none() { none_timer_->start(); }
  void cancel_none() { none_timer_->stop(); }

  void await_idle() { idle_timer_->start(); }
  void cancel_idle() { idle_timer_->stop(); }

  void print_state();

 private slots:
  void on_none_timeout();
  void on_idle_timeout();

  void on_sigusr1();
  void on_sigusr2();

 private:
  Filewatcher* settings_watcher_ = nullptr;
  QSettings  * settings_ = nullptr;

  State state_ = State::NONE;

  QTimer* none_timer_ = nullptr;
  QTimer* idle_timer_ = nullptr;

  Signal* sigusr1_    = nullptr;
  Signal* sigusr2_    = nullptr;
};

}  // namespace clac
}  // namespace steamlink
}  // namespace lvd
